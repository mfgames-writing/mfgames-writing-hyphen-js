# Hyphenation plugin support for MfGames Writing (Javascript).

This is a pipeline module which applies hyphenation to source files in a consistent manner. It updates the text to use numerical version of `&shy;` to indicate where a word could be broken if appropriate.
