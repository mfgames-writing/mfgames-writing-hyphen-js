let expect = require("expect");
import { HyphenPlugin, loadHyphenPlugin } from "../hyphen";
import { ContentArgs, EditionArgs, ContentData, EditionData, PublicationArgs } from "@mfgames-writing/contracts";
import "mocha";

describe("parsing", function() {
    let plugin = loadHyphenPlugin({
        exclude: ["`.*?`"]
    });

    it("simple word", function(done) {
        // Create the input we'll be processing.
        let publicationArgs = new PublicationArgs(null, null, null);
        let editionData = {} as EditionData;
        let editionArgs = new EditionArgs(publicationArgs, "test", editionData);
        let contentData = {} as ContentData;
        let contentArgs = new ContentArgs(editionArgs, contentData);

        contentArgs.text = "hyphenate";

        // Create the plugin and process data using it.
        let promise = plugin.process(contentArgs);

        promise.then((c) => {
            const text = c.text.replace(/&#173;/g, "|");
            expect(text).toEqual("hy|phen|ate");
            done();
        });
    });

    it("single attribute", function(done) {
        // Create the input we'll be processing.
        let publicationArgs = new PublicationArgs(null, null, null);
        let editionData = {} as EditionData;
        let editionArgs = new EditionArgs(publicationArgs, "test", editionData);
        let contentData = {} as ContentData;
        let contentArgs = new ContentArgs(editionArgs, contentData);

        contentArgs.text = "hyphenate {:.center}";

        // Create the plugin and process data using it.
        let promise = plugin.process(contentArgs);

        promise.then(c => {
            let text = c.text.replace(/&#173;/g, "|");
            expect(text).toEqual("hy|phen|ate {:.center}");
            done();
        });
    });

    it("multiple attribute", function(done) {
        // Create the input we'll be processing.
        let publicationArgs = new PublicationArgs(null, null, null);
        let editionData = {} as EditionData;
        let editionArgs = new EditionArgs(publicationArgs, "test", editionData);
        let contentData = {} as ContentData;
        let contentArgs = new ContentArgs(editionArgs, contentData);

        contentArgs.text = "hyphenate {:.center}\n\nchapter\n\nhyphenate {:.center}\n\nchapter";

        // Create the plugin and process data using it.
        let promise = plugin.process(contentArgs);

        promise.then(c => {
            let text = c.text.replace(/&#173;/g, "|");
            expect(text).toEqual("hy|phen|ate {:.center}\n\nchap|ter\n\nhy|phen|ate {:.center}\n\nchap|ter");
            done();
        });
    });

    it("code blocks", function(done) {
        // Create the input we'll be processing.
        let publicationArgs = new PublicationArgs(null, null, null);
        let editionData = {} as EditionData;
        let editionArgs = new EditionArgs(publicationArgs, "test", editionData);
        let contentData = {} as ContentData;
        let contentArgs = new ContentArgs(editionArgs, contentData);

        contentArgs.text = "hyphenate `hyphenate` hyphenate";

        // Create the plugin and process data using it.
        let promise = plugin.process(contentArgs);

        promise.then(c => {
            let text = c.text.replace(/&#173;/g, "|");
            expect(text).toEqual("hy|phen|ate `hyphenate` hy|phen|ate");
            done();
        });
    });
});
