## [0.4.3](https://gitlab.com/mfgames-writing/mfgames-writing-hyphen-js/compare/v0.4.2...v0.4.3) (2018-08-09)


### Bug Fixes

* adding management packages ([0cac35d](https://gitlab.com/mfgames-writing/mfgames-writing-hyphen-js/commit/0cac35d))
